<?php

declare(strict_types=1);

namespace App\Controller;
use App\Service\TodoService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController
{
    public function index(TodoService $todoService): JsonResponse
    {

        $todoService->addTodo();
        
        return new JsonResponse([
            [
                'id' => 1,
                'title' => 'test 1',
                'completed' => false,
            ],
        ], 200, ['Access-Control-Allow-Origin' => '*']);
        

    }
    /**
     * @Route("/delete", name="admin_index", methods={"DELETE"})
     */
    public function deleteTodo(TodoService $todoService): JsonResponse
    {
        $todoService->delTodo();
        return new JsonResponse([
            [
                'deleted' => true
            ],
        ], 200, ['Access-Control-Allow-Origin' => '*']);
    }
}

