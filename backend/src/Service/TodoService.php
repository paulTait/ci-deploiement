<?php

namespace App\Service;

use App\Repository\TodoRepository;

class TodoService {

    private $todoRepository;

    public function __construct(TodoRepository $todoRepository){
        $this->todoRepository = $todoRepository;
    }

    public function addTodo() {
        $this->todoRepository->addTodo();
    }

    public function delTodo() {
        $this->todoRepository->delTodo();
    }
}